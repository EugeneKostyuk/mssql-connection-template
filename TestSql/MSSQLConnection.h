#ifndef  _MSSQL_CONNECTION_
#define _MSSQL_CONNECTION_

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace Data;
using namespace Data::SqlClient;


namespace MSSQL
{
	//Класс только для подключения к БД
	ref class Connection
	{
		private: String^ host; //Адрес базы
		private: String^ dbName; //Имя базу

		private: SqlConnection^ connection; //Объект подключения
		//Удобный конструктор строки подключения
		private: SqlConnectionStringBuilder^ stringBuilder;

		public: Connection(String^ host, String^ dbName)
		{
			//Делаем так, что бы убрать мусор
			//Т.к. должно быть NULL, мы никуда не подключились еще...
			connection = nullptr;

			//В этом методе(Конструктор) мы получем знаечения и...
			// ...готовим строку подлкючения к БД
			this->host = host;
			this->dbName = dbName;

			stringBuilder = gcnew SqlConnectionStringBuilder();
			stringBuilder->DataSource = host;
			stringBuilder->InitialCatalog = dbName;
			stringBuilder->IntegratedSecurity = true;
		}

		public: bool startConnection()
		{
			//Результат подключения
			bool connected = false;
			try {
				//Пытаемся подключится к Базе Данных
				connection = gcnew SqlConnection(stringBuilder->ToString());
				connection->Open();
				
				//Если мы дошли сюда, значит все ок и мы возвращаем true
				connected = true;
				return connected;
			}
			catch (...)
			{
				//Если мы попали сюда, значит подключится не вышло
				//Возвращаем false
				return connected;
			}
		}

		//Можно получить подключение, которое было создано
		public: SqlConnection^ getConnectionObject()
		{
			return connection;
		}
		
		//Закрыть подключение к БД
		//Использовать каждый раз, когда подключение больше не нужно
		public: bool stopConnection()
		{
			//Результат отключения 
			bool disconnected = false;
			try {
				//Пытаемся отключится
				connection->Close();

				//Если все ок - возвращаем true
				disconnected = true;
				return disconnected;
			}
			catch (...)
			{
				//Если попали сюда, значит по какой-то причине
				// отключится не получилось
				//Скорее всего просто подключения и не было и там NULL
				return disconnected;
			}
		}
	};
}
#endif // ! _MSSQL_CONNECTION_

