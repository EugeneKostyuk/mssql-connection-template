#ifndef _USERS_CONTEXT_
#define _USERS_CONTEXT_

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace Data;
using namespace Data::SqlClient;


namespace ConsoleUsersDB
{
	//Просто удобный класс 
	// для хранения данных о пользователе
	ref class User
	{
		public: int Id;
		public: String^ Login;
		public: String^ Password;

		//Три удобных конструктора
		//Пустой объект
		public: User()
		{
			Id = 0;
			Login = String::Empty;
			Password = String::Empty;
		}

		//При создании определить Логин и Пароль
		// (нужно при Insert, там нам не нужен Id...
		// ...т.к. Id создается автоматически при добавлении)
		public: User(String^ login, String^ password)
		{
			Id = 0;
			Login = login;
			Password = password;
		}

		//Полный конструктор
		//Например если нужно передать в Delete or Update or Select
		// где может понадобиться знаечение ID
		// которое мы где-то уже получим ранее
		public: User(int id, String^ login, String^ password)
		{
			Id = id;
			Login = login;
			Password = password;
		}
	};

	ref class UserContext
	{
		//Объект подключения к базе данных
		private: SqlConnection^ connection;

		//Удобный конструктор для инициализации объекта подключения
		public: UserContext(SqlConnection^ connection)
		{
			this->connection = connection;
		}

		//Добавления в базу пользователя со значениями
		// которые хранятся в объекте user
		public: bool Insert(User% user)
		{
			//Готовим результат выполения
			bool its_ok = false;
			try {
				String^ query = "INSERT INTO dbo.Users(Users.Login, Users.Password) " +
					"VALUES(@Login, @Password)";
				SqlCommand^ command = gcnew SqlCommand(query, connection);
				command->Parameters->Add("@Login", user.Login);
				command->Parameters->Add("@Password", user.Password);

				//Если все ОК, то из функции вернется true
				command->ExecuteNonQuery();
				its_ok = true;
				return its_ok;
			}
			catch (...)
			{
				//Но если мы попали сюда, значит что-то пошло не так :)
				//И мы вернем false
				return its_ok;
			}
		}

		//Получение результата списка всех пользователей
		public: SqlDataReader^ getAllUsers()
		{
			//Готовим переменную для результата запроса
			SqlDataReader^ resultReader = nullptr;

			try {
				//Строка запроса и подготовка команды
				String^ query = "SELECT * FROM Users;";
				SqlCommand^ command = gcnew SqlCommand(query, connection);

				//Выполняем запрос и сохраняем результат в resultReader
				resultReader = command->ExecuteReader();
				
				//Возвращаем переменную с результатом запроса
				return resultReader;
			}
			catch (...)
			{
				//Если мы попали сюда, значит запрос не выполнился
				//resultReader осталась NULL
				// -> Возвращаем результатом NULL
				return resultReader;
			}
		}		
	};
}
#endif