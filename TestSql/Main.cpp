#include "MSSQLConnection.h"
#include "UsersContext.h"

using namespace System::Data;
using namespace System::Data::SqlClient;

//Строка подключение к базе из свойств базу данных
//Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ConsoleUsersDB;Integrated Security=True
int main()
{
	//Настройки подключения
	String^ host = "(localdb)\\MSSQLLocalDB";
	String^ dbName = "ConsoleUsersDB";

	//Создание объекта подключения
	MSSQL::Connection^ connect = gcnew MSSQL::Connection(host, dbName);
	
	//Попытка подключится
	if (connect->startConnection())
	{
		//Заменяю вывод на консоль с помощью класса
		// который поддерживает System::String
		Console::WriteLine("Is Connected!");
	}
	else
	{
		//Заменяю вывод на консоль с помощью класса
		// который поддерживает System::String
		Console::WriteLine("Is not Connected!");
		return 0;
	}

	//Создания объекта контекста
	//С помощью этого контекста мы общаемся с таблицей в бд
	//Он работает только с готовым connection to Data Base
	ConsoleUsersDB::UserContext^ users = 
		gcnew ConsoleUsersDB::UserContext(connect->getConnectionObject());

	//Создаю новый объект Пользователя
	ConsoleUsersDB::User user(0, "Dima", "654321");
	
	//Пытаюсь добавить его в базу
	if (users->Insert(user))
	{
		//Заменяю вывод на консоль с помощью класса
		// который поддерживает System::String
		Console::WriteLine("User was added!");
	}
	else
	{
		//Заменяю вывод на консоль с помощью класса
		// который поддерживает System::String
		Console::WriteLine("User was not added!");
	}
	//New Line
	Console::WriteLine();

	//Получаем результат запроса SELECT * FROM Users
	// и записываем его в reader, что бы дальше прочитать его
	SqlDataReader^ reader = users->getAllUsers();

	//Если в результате что-то есть
	if (reader != nullptr)
	{
		//Получаем кол-во столбцов к результате
		//Кол-во строк не известно до чтения
		int numberCol = reader->FieldCount;

		//Вывожу шапку таблицы
		//Получаю имя стобца и вывожу на экран в цикле
		//Использую кол-во столцов, которе получил заранее numberCol
		for (int i = 0; i < numberCol; i++)
		{
			Console::Write(reader->GetName(i)->ToString() + "\t");
		}

		//Переход на новую строку
		Console::WriteLine();

		//Дальше идем по reader и выводим ввиде таблицы
		//Пока есть что читать
		//Пока остались строки результата запроса
		while (reader->Read())
		{
			//reader на каждом проходе цикла while -
			// это массив значений одной строки подряд
			// в той последовательности, в которой мы их вызвали в запросе

			//Вывожу в цикле значения результата
			for (int i = 0; i < numberCol; i++)
			{
				Console::Write(reader[i]->ToString() + "\t");
			}

			//Переход на новую строку как при выводе матрицы
			Console::WriteLine();
		}
	}
	else
	{
		Console::WriteLine("Результатов запроса нет!");
	}

    //Закрываем подключение
	connect->stopConnection();
	
	//Задержка выполнения программы до нажатия Enter
	Console::ReadLine();

	return 0;
}